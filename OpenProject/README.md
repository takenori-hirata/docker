`OpenProject`の`docker-compose`

```yaml
# docker-compose.yml

version: '3'

services:
  openproject:
    image: openproject/community:latest
    ports:
      - "30080:80"
    volumes:
      - ./db-data:/var/lib/postgresql/data
      - ./data:/var/db/openproject
    environment:
      SECRET_KEY_BASE: openproject_secret_key
```

コマンド  
`$ docker-compose up -d`  
  
URL  
http://localhost:30080/  
  

参考  
https://qiita.com/terukizm/items/c94f06a40be55f65d4f9  
https://www.openproject.org/docker/  